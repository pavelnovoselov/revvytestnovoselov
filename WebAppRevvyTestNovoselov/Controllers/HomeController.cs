﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebAppRevvyTestNovoselov.Models;

namespace WebAppRevvyTestNovoselov.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Формируем массив случайных чисел от 1 до 9 размерностью num
        /// </summary>
        [HttpPost]
        public ActionResult<TestData> ArrayFormation(int num)
        {
            var result = new TestData
            {
                ArrayNumbers = new int[num]
            };

            var r = new Random();

            for (int i = 0; i < num; i++)
            {
                result.ArrayNumbers[i] = r.Next(1, 9);
            }
            return result;
        }

        /// <summary>
        ///  Определяем, можно ли число num представить суммой чисел из массива
        /// </summary>
        [HttpPost]
        public ActionResult<string> GetTestResult(int[] arrayNumbers, int num)
        {
            var result = "";

            // сокращаем массив на примитивные несовпадения
            var allSum = 0;
            var verifiedNumbers = new List<int>();
            if (arrayNumbers.Length == 1)
            {
                return arrayNumbers[0] == num ? $"Число {num} - единственный элемент массива" : "";
            }
            foreach (var item in arrayNumbers)
            {
                allSum += item;
                if (item < num)
                {
                    verifiedNumbers.Add(item);
                }
            }
            if (verifiedNumbers.Count > 0 && allSum >= num)
            {
                if (allSum == num)
                {
                    return $"Число {num} - можно представить суммой всех элементов массива";
                }
                GetResult(ref result, verifiedNumbers, num, new List<int>());
            }

            return result;
        }

        /// <summary>
        ///  Рекурсивный перебор массива
        /// </summary>
        private void GetResult(ref string result, List<int> numbers, int target, List<int> partial)
        {
            int sum = 0;
            foreach (int p in partial)
            {
                sum += p;
            }

            if (sum == target)
            {
                result = $"Число {target} - можно представить суммой ({string.Join(" + ", partial.ToArray())})";
                return;
            }

            if (sum >= target)
            {
                return;
            }

            for (int i = 0; i < numbers.Count; i++)
            {
                List<int> remaining = new();

                for (int j = i + 1; j < numbers.Count; j++)
                {
                    remaining.Add(numbers[j]);
                }

                List<int> partial_rec = new(partial)
                {
                    numbers[i]
                };

                if (result == "")
                {
                    GetResult(ref result, remaining, target, partial_rec);
                }                
            }
        }



    }
}