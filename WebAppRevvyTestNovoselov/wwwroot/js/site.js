﻿
function checkNumber() {
    // разрешаем вводить только натуральные числа
    if (event.keyCode < 48 || event.keyCode > 57)
        event.preventDefault();
}

function arrayFormation() {    
    let resultTest = document.getElementById('test_result');
    resultTest.innerHTML = '';
    let showarray = document.getElementById('show_array');
    showarray.innerHTML = '';
    let message = document.getElementById('selected_array_dimension_error');
    message.innerHTML = '';
    let message2 = document.getElementById('selected_check_number_error');
    message2.innerHTML = '';
    document.getElementById('selected_numbers').value = '';

    // валидация длины массива
    let val = document.getElementById('array_dimension').value
    let num = parseInt(val);
    if (num < 1 || val == '') {
        message.innerHTML = 'Длина массива должна быть больше нуля';
        return;
    }
    let data = {
        Num: num
    }

    $.ajax({
        url: "/Home/ArrayFormation",
        data: data,
        type: "POST",
    }).done(function (result) {

        document.getElementById('formed_array').value = result.arrayNumbers;

        let str = "Получен массив: { ";
        for (var i = 0; i < result.arrayNumbers.length; i++) {
            str += result.arrayNumbers[i] + ", ";
        }
        str += " }"
        let showarray = document.getElementById('show_array');
        showarray.innerHTML = str;
        $('#check_block').show();
    });
}

function getTestResult() {
    let resultTest = document.getElementById('test_result');

    // валидация введенного числа
    let val = document.getElementById('check_number').value
    let num = parseInt(val);
    let message = document.getElementById('selected_check_number_error');
    message.innerHTML = '';    
    if (val == '') {
        message.innerHTML = 'Введите число';
        resultTest.innerHTML = '';
        return;
    }

    // каждое число разрешаем использовать только один раз
    let selectedNumbersEl = document.getElementById('selected_numbers');
    let selectedNumbers = selectedNumbersEl.value;

    let strNumbers = selectedNumbers.split(',');
    for (var i = 0; i < strNumbers.length - 1; i++) {
        if (strNumbers[i] == val) {
            message.innerHTML = `Каждое число можно использовать один раз, число ${num} уже использовали`;
            resultTest.innerHTML = '';
            return;
        }
    }
    selectedNumbersEl.value += val + ',';
    
    // считывание массива
    let formedArray = document.getElementById('formed_array').value;
    let strArrays = formedArray.split(',');
    let arrayNumbers = [];
    for (var i = 0; i < strArrays.length; i++) {
        arrayNumbers.push(parseInt(strArrays[i]));
    }

    let data = {
        ArrayNumbers: arrayNumbers,
        Num: num
    }

    $.ajax({
        url: "/Home/GetTestResult",
        data: data,
        type: "POST",
    }).done(function (result) {        
        resultTest.innerHTML = result == '' ? `Число ${num} - нельзя` : result;
    });
}
